#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cstdarg>
#include <ctime>
#include <dirent.h>
#include <sys/types.h>
#include <stdio.h>

class CSV {
	public:
		std::vector< std::vector <std::string> > values;
		std::string filename;
		CSV(const char* filename) {
			this->filename = filename;
			std::ifstream file;
			file.open(filename);
			if(file.is_open()) {
				std::string line;
				while(getline(file, line)) {
					std::string lineS = line;
					std::vector<std::string> lineData;
					std::stringstream ss(lineS);
					
					while(ss.good()) {
						std::string substr;
						getline(ss, substr, ',');
						lineData.push_back(substr);
					}
					values.push_back(lineData);
				}
			}
			file.close();
			if(values.size() > 0) {
			if(values.at(0) != proper1Row) {
				values.insert(values.begin(), proper1Row);
				WriteFile();
			}
			}


		}
		int AddRow(std::string classS, std::string assignment, std::string due) {
			std::vector<std::string> row = {classS, assignment, due, ""};
			values.push_back(row);
			return 0;
		}
		int WriteFile() {
			std::ofstream file(filename);
			if(file.is_open()) {
				for(int nRow = 0; nRow < values.size(); nRow++){
					std::vector<std::string> rowData = values[nRow];
					std::string rowString;
					for(int nColumn = 0; nColumn < rowData.size(); nColumn++){
						std::string extra;
						if(rowData.size() !=  nColumn+1)
							extra = ",";
						rowString.append(rowData.at(nColumn) + extra);
					}
					file << rowString << std::endl;
				}
			}
			file.close();
			return 0;
		}
		int PrintList() {
			if(values.size() > 0) {
				printf("\n	");
				for(int nColumn = 0; nColumn < values.at(0).size(); nColumn++) {
					printf("	%s	", values.at(0).at(nColumn).c_str());
				}
				printf("\n	-----------------------------------------------------------------------------\n");
				if(values.size() > 1) {
					for(int nRow = 1; nRow < values.size(); nRow++) {
						for(int nColumn = 0; nColumn < values.at(nRow).size(); nColumn++) {
							std::cout << "		" <<  values.at(nRow).at(nColumn).c_str();
						}
						printf("\n");
					}	
				}
			} else {
				printf("Nothing for today :) \n");
			}
			return 0;
		}
	private:
		std::vector<std::string> proper1Row= {"Class","Assignments","Due", "Completed"}; 
};

class HTMLTable {
	public: 
		HTMLTable(std::string Heading){
			top = "<!DOCTYPE HTML><html><head><link rel='stylesheet' href='main.css' /></head><body><h1>" + Heading + "</h1><table>";

		}
		std::string GetHTML(std::string tableData) {
			return top + tableData + bottom;
		}
		std::string HeadingString(std::vector<std::string> args){
			std::string result;
			result.append("<tr>");
			for(int i = 0; i < args.size(); i++){
				result.append("<th>" + args.at(i) + "</th>");
			}
			result.append("</tr>");
			return result;
		}
		std::string EntryString(std::vector<std::string> args) {
			std::string result = "<tr>";
			for(int i = 0; i < args.size(); i++){
				result.append("<td>" + args.at(i) + "</td>");
			}
			result.append("</tr>");
			return result;
		}
		int WriteFile(std::string filename, std::string data) {
			std::ofstream file;
			file.open(filename);
			if(file.is_open()) {
				file << data;
			}
			file.close();
			return 0;
		}

	private:
		std::string top;
		std::string bottom = "</table></body></html>";
};

class Time {
	public:
		Time() {

		}
		int year() {
			tm *ltm = getDetails();
			return 1900 + ltm->tm_year;
		}
		int month() {
			tm *ltm = getDetails();
			return 1 + ltm->tm_mon;
		}
		int day() {
			tm *ltm = getDetails();
			return ltm->tm_mday;
		}
	private:
		tm* getDetails() {
			time_t now = time(0);
			return localtime(&now);
		}
};

enum verbs {
	NONE,
	ADD,
	HTML,
	LIST
};

int str2verb(std::string arg) {
	if(arg == "add") return verbs::ADD;
	if(arg == "html") return verbs::HTML;
	if(arg == "list") return verbs::LIST;
	else return verbs::NONE;
}

std::string verb2str(verbs verb) {
	if(verb == ADD) return "add";
	if(verb == HTML) return "html";
	if(verb == LIST) return "list";
}

std::string dateFilename(Time *time, std::string format) {
	std::string month = (time->month() < 10) ? "0" + std::to_string(time->month()) : std::to_string(time->month());
	std::string day = (time->day() < 10) ? "0" + std::to_string(time->day()) : std::to_string(time->day());
	std::string dateFile = std::to_string(time->year()) + month + day + format;
	return dateFile;
}


int main(int argc, char* argv[]) {
	Time *time = new Time();
	std::string rootDir = "/home/beck/assignments/";
	HTMLTable *table = new HTMLTable(std::to_string(time->year()) + "-" + std::to_string(time->month()) + "-" + std::to_string(time->day()));
//	printf("Ran: %i-%i-%i \n",time->year(), time->month(), time->day()); 
//	std::cout << argc << std::endl;
	if(argc > 1) {
		int verb = str2verb(std::string(argv[1]));
		switch(verb) {
			case verbs::ADD:
				{
				CSV *list;
				if(argc > 5 && argc < 6) {
					std::string timeInput = rootDir + std::string(argv[5]) + ".csv";
					list = new CSV(timeInput.c_str());	
				} else {
					std::string dateFile = rootDir + dateFilename(time, ".csv");
					std::cout << "Filename: " << dateFile << std::endl;
					list = new CSV(dateFile.c_str());
				}
				if(argc > 2 && argc < 4) {
					printf("%s %s %s <assignment> [due] [date]\n",argv[0],argv[1],argv[2]);
				} else if(argc <= 2) {
					printf("%s %s <class> <assignment> [due] [date]\n",argv[0],argv[1]);
				}else {
					std::string due = "";
					if(argc > 4) {
						due = std::string(argv[4]);
					}
					list->AddRow(argv[2], argv[3], due);
					list->WriteFile();
				}
				list->PrintList();
				}
				break;
			case verbs::HTML:
				{
					std::string csvFilename = rootDir + dateFilename(time, ".csv");
					CSV *list = new CSV(csvFilename.c_str());
					std::string filename = rootDir + dateFilename(time, ".html");
					std::string dom;
					if(list->values.size() > 0) {
						dom.append(table->HeadingString(list->values.at(0)));
						if(list->values.size() > 1) {
							for(int nRow = 1; nRow < list->values.size(); nRow++) {
								dom.append(table->EntryString(list->values.at(nRow)));
							}
						}
						dom = table->GetHTML(dom);
					} else {
						dom = "No content available";
					}
					table->WriteFile(filename, dom);
				}	
				break;
			case verbs::LIST:
				{
					std::string filename = rootDir + dateFilename(time, ".csv");
				CSV *list = new CSV(filename.c_str());
				list->PrintList();
				}
			default:
				break;
		}
	}
	return 0;
}
